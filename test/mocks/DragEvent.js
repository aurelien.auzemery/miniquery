// This mock class is needed for now, because jsdom does not implement it
export default class DragEvent extends MouseEvent {
  constructor(type, DragEventInit) {
    super(type, DragEventInit);
    this.dataTransfer = DragEventInit.dataTransfer ?? null;
  }
}

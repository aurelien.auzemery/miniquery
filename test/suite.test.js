// Needed mock, DragEvent isn't implemented in jsdom
import DragEvent from "./mocks/DragEvent.js";
window.DragEvent = DragEvent;

// $() imports from the code to be tested
import $ from "../src/miniQuery.js";

// Test suites to be tested
import dollar from "./unit/dollar.js";
import eventHandling from "./unit/eventHandling.js";
import traversal from "./unit/traversal.js";

describe("Testing queries", () => {
  dollar($);
});

describe("Testing event handling", () => {
  eventHandling($);
});

describe("Testing traversal methods", () => {
  traversal($);
});

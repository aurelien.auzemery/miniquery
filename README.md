## miniQuery

**Yet another lightweight jQuery clone.**

This is originally a studying project to better understand the mechanics behind the jQuery library, while learning how to use modern tools to build and maintain such a project.

The goal is to reproduce the main jQuery features, but with ES6+. As such, please understand that **compatibility with older browsers is not intended**. Again, **this is a studying project**. This library doesn't strive for ultimate performance, optimization, and compatibility. **I would not recommend using miniQuery for production projects.** See [alternatives](#alternatives) below.

Helping with code reviewing, proposing new features, or anything else, is welcome!

---

## Getting started

Simply grab the latest release, or clone the repo and build into the [/dist/](https://gitlab.com/miniquery/miniquery/-/tree/main/dist) folder.

There are three different builds you can choose from:

- ES6 module build (`.mjs`): meant for polyvalent use in NodeJS or the browser. Minified version also available.

```js
import $ from "./miniQuery.mjs";
```

- CommonJS build (`.cjs`): meant for use inside NodeJS.

```js
const $ = require("miniQuery.cjs");
```

- IIFE build (`.js`): meant for browser use only. Automatically creates a global variable named `$`. Minified version also available.

```html
<script src="miniQuery.js"></script>
```

---

## Features

You can access [the full documentation from the project's GitLab Pages](https://miniquery.gitlab.io/miniquery/doc/index.html). Click on a feature to extend a summary list of implemented functions.

<details>
    <summary>Selector function (<b>$</b>)</summary>
        <ul>
            <li>Grab the document in a NodeArray with `$()`</li>
            <li>Select nodes with a string selector</li>
            <li>Convert a NodeList into a NodeArray</li>
            <li>Put an already defined Node into its own NodeArray</li>
        </ul>
</details>

<details>
    <summary>Event handling</summary>
        <ul>
            <li><b>on()</b>: attach event listeners</li>
            <li><b>one():</b> attach event listeners to execute once</li>
            <li><b>off():</b> remove event listeners</li>
            <li><b>trigger():</b> trigger an event</li>
            <li><b>ready():</b> execute a function once the document is ready</li>
        </ul>
</details>

<details>
    <summary>DOM traversal</summary>
        <ul>
            <li><b>.add()</b> </li>
            <li><b>.addBack()</b> </li>
            <li><b>.children()</b> </li>
            <li><b>.closest()</b> </li>
            <li><b>.contents()</b> </li>
            <li><b>.end()</b> </li>
            <li><b>.eq()</b> </li>
            <li><b>.even()</b> </li>
            <li><b>.filter()</b> </li>
            <li><b>.find()</b> </li>
            <li><b>.first()</b> </li>
            <li><b>.has()</b> </li>
            <li><b>.is()</b> </li>
            <li><b>.last()</b> </li>
            <li><b>.map()</b> </li>
            <li><b>.next()</b> </li>
            <li><b>.nextAll()</b> </li>
            <li><b>.nextUntil()</b> </li>
            <li><b>.not()</b> </li>
            <li><b>.odd()</b> </li>
            <li><b>.parent()</b> </li>
            <li><b>.parents()</b> </li>
            <li><b>.parentsUntil()</b> </li>
            <li><b>.prev()</b> </li>
            <li><b>.prevAll()</b> </li>
            <li><b>.prevUntil()</b> </li>
            <li><b>.siblings()</b> </li>
            <li><b>.slice()</b> </li>
        </ul>
</details>

---

## Planned features

- Generating Nodes with $("plainHTML");
<details>
    <summary>DOM manipulation</summary>
        <ul>
            <li><b>.addClass()</b> </li>
            <li><b>.after()</b> </li>
            <li><b>.append()</b> </li>
            <li><b>.appendTo()</b> </li>
            <li><b>.attr()</b> </li>
            <li><b>.before()</b> </li>
            <li><b>.clone()</b> </li>
            <li><b>.css()</b> </li>
            <li><b>.detach()</b> </li>
            <li><b>.empty()</b> </li>
            <li><b>.hasClass()</b> </li>
            <li><b>.html()</b> </li>
            <li><b>.insertAfter()</b> </li>
            <li><b>.insertBefore()</b> </li>
            <li><b>.prepend()</b> </li>
            <li><b>.prependTo()</b> </li>
            <li><b>.prop()</b> </li>
            <li><b>.remove()</b> </li>
            <li><b>.removeAttr()</b> </li>
            <li><b>.removeClass()</b> </li>
            <li><b>.removeProp()</b> </li>
            <li><b>.replaceAll()</b> </li>
            <li><b>.replaceWith()</b> </li>
            <li><b>.text()</b> </li>
            <li><b>.toggleClass()</b> </li>
            <li><b>.unwrap()</b> </li>
            <li><b>.val()</b> </li>
            <li><b>.wrap()</b> </li>
            <li><b>.wrapAll()</b> </li>
            <li><b>.wrapInner()</b> </li>
            <li><b>jQuery.cssNumber</b> </li>
            <li><b>jQuery.htmlPrefilter()</b> </li>
        </ul>
</details>

---

## Contributing

- Clone the repository to your local workspace.
- Run `npm install` to get all the necessaty development dependencies.
- Implemented features need unit testing with jest. To add your own tests, create them in the `/test/unit/` folder, and import your created file in `testSuite.js`. You can then run `npm run all`.
- Documentation follows the JSDoc formatting.

Project workflow:

- Linting is done with [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/): `npm run lint`
- Building is done with [Rollup](https://rollupjs.org/guide/en/): `npm run build`
- Testing is done with [Jest](https://jestjs.io/): `npm run test`
- The three steps above can be done sequentially in a **single command**: `npm run all`
- The documentation is generated with JSDoc: `npm run doc`
- Every pushed commit is tested in a CI pipeline. Every commit on main also regenerates documentation and readies it for the project's GitLab Pages.

---

## <a id="alternatives"></a> Alternatives

- [jQuery](https://jquery.com/)

- [UmbrellaJS](https://umbrellajs.com/)

- [Zepto](http://zeptojs.com/)

- [Bliss](http://blissfuljs.com/)

- [NodeList](https://github.com/eorroe/NodeList.js)

- [Micro Framework (many)](http://microjs.com/)

---

## Author and License

Created and maintained by Aurélien AUZEMERY under the [MIT license](https://gitlab.com/miniquery/miniquery/-/blob/main/LICENSE).

---

## Sources of inspiration

- [You might not need jQuery](https://youmightnotneedjquery.com/)
- [UmbrellaJS](https://umbrellajs.com/)
- [WebDev Simplified - Stop Using jQuery - How To Create Your Own jQuery Clone](https://www.youtube.com/watch?v=5MFnKG15ZfI)
